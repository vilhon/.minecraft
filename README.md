# TITANICRAFT #

### What is this repository for? ###

* Store and keep track of all the changes of the Minecraft recreation of the RMS Titanic.

* Minecraft version: `1.5.2`.

# Getting Started
## Dependencies
What you need to run this project:

* Computer running `Windows`.

* `Java JRE 6` or above

## Installation

1) [Download](https://bitbucket.org/vilhon/titanicraft/raw/d39206a88064210ffecb26f4044392111f37c373/Minecraft%201.5.2.exe) the Minecraft executable and follow the instructions.

2) The contents of this repo should be placed on the folder `C:\Users\<username>\AppData\Roaming\.minecraft`, where the executable will automatically access. It is recommended this way, since this way, since this will allow to keep constant track of changes and commit at any time.

3) Try running the `Minecraft 1.5.2.exe`. If any Java related error appears, make sure you download the Java JRE Package from the Oracle site.

## Running the project (game)
1) Once placed the contents, run the `Minecraft 1.5.2.exe` and wait for the main screen to show.

2) Head to Single Player and run the world `TITANIC`. If everything went right, the world textures should look consistent.

### Who do I talk to? ###

* Owner: Pedro Solis Garcia (solisgpedro@gmail.com)